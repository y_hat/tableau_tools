"""
Author:         Victor Faner
Date Created:   2019-04-09
Description:    Module to pull usage statistics from Tableau Server
                Workbooks
"""

import logging
from getpass import getpass
from pathlib import Path
from configparser import ConfigParser

import pandas as pd
import tableauserverclient as tsc
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

#  Set which Projects to pull data from
PROJECTS = []
LOGS = Path.cwd().joinpath('logs')
OUTPUT = Path.cwd().joinpath('output')
CONFIG = Path.cwd().joinpath('config.cfg')


def get_project_ids(server):
    """
    Get list of project ids to query

    :param server:          authenticated tableauserverclient.Server
                            instance
    :return project_list:   List of projects to query
    """
    logging.info('Getting projects to query. ')
    assert isinstance(server.projects.get, object)
    all_project_items, pagination_item = server.projects.get()
    project_list = [
        project.id for project in all_project_items
        if project.name in PROJECTS
    ]

    return project_list


def get_view_stats(server, workbook):
    """
    Helper function to get data and usage statistics on individual views

    :param server:          authenticated tableauserverclient.Server
                            instance
    :param workbook:        tableauserverclient WorkbookItem instance
    :return project_list:   List of projects to query
    """
    server.workbooks.populate_views(workbook, usage=True)
    view_data = []

    for view in workbook.views:
        view_data.append(workbook.name)
        view_data.append(view.workbook_id)
        view_data.append(view.name)
        view_data.append(view.id)
        view_data.append(view.content_url)
        view_data.append(workbook.project_id)
        view_data.append(workbook.project_name)
        view_data.append(workbook.owner_id)
        view_data.append(view.total_views)

    return view_data


def get_workbook_stats(server, project_list):
    """
    Get data and usage statistics on each workbook

    :param server:          authenticated tableauserverclient.Server
                            instance
    :param project_list:    List of projects to query
    :return workbook_stats: Dict of workbooks to query
    """
    assert isinstance(server.workbooks.get, object)
    workbook_stats = {
        'workbook_name': [],
        'workbook_id': [],
        'view_name': [],
        'view_id': [],
        'content_url': [],
        'project_id': [],
        'project_name': [],
        'owner_id': [],
        'total_views': [],
    }

    for workbook in tsc.Pager(server.workbooks):
        if workbook.project_id in project_list:
            view_data = get_view_stats(server, workbook)

            workbook_stats['workbook_name'].append(view_data[0])
            workbook_stats['workbook_id'].append(view_data[1])
            workbook_stats['view_name'].append(view_data[2])
            workbook_stats['view_id'].append(view_data[3])
            workbook_stats['content_url'].append(view_data[4])
            workbook_stats['project_id'].append(view_data[5])
            workbook_stats['project_name'].append(view_data[6])
            workbook_stats['owner_id'].append(view_data[7])
            workbook_stats['total_views'].append(view_data[8])

            logging.info(f'Successfully queried data for workbook: '
                         f'{view_data[0]}')
        else:
            continue

    return workbook_stats


def main():
    LOGS.mkdir(exist_ok=True)
    OUTPUT.mkdir(exist_ok=True)
    filename = Path.cwd().joinpath('logs', 'get_usage_stats.log')
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s: %(message)s',
        handlers=[logging.FileHandler(filename), logging.StreamHandler()]
    )

    cfg = ConfigParser()
    cfg.read(CONFIG)

    username = input('Please enter your Tableau Server username: ')
    password = getpass('Please enter your Tableau Server password: ')

    tableau_auth = tsc.TableauAuth(username, password)
    server = tsc.Server(cfg['SERVER']['URL'])

    #  Disable SSL verification to allow sign-in
    server.add_http_options({'verify': False})
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    with server.auth.sign_in(tableau_auth):
        project_list = get_project_ids(server)
        workbook_stats = get_workbook_stats(server, project_list)

    logging.info('Writing Workbook data to .csv')
    workbook_df = pd.DataFrame(workbook_stats)
    workbook_df.to_csv(
        Path.cwd().joinpath('output', 'workbook_stats.csv'),
        index=False
    )


if __name__ == '__main__':
    main()
